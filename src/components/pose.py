import cv2
import numpy as np


#class Pose:
class Pose:
    #attributes ------------------------------------------------------------------------------------------------------

    BODY_PARTS = { "nose": 0, "neck": 1, "rshoulder": 2, "relbow": 3, "rwrist": 4,
                "lshoulder": 5, "lelbow": 6, "lwrist": 7, "rhip": 8, "rknee": 9,
                "rankle": 10, "lhip": 11, "lknee": 12, "lankle": 13, "reye": 14,
                "leye": 15, "rear": 16, "lear": 17, "background": 18 }

    POSE_PAIRS = [ ["neck", "rshoulder"], ["neck", "lshoulder"], ["rshoulder", "relbow"],
                 ["relbow", "rwrist"], ["lshoulder", "lelbow"], ["lelbow", "lwrist"],
                 ["neck", "rhip"], ["rhip", "rknee"], ["rknee", "rankle"], ["neck", "lhip"],
                 ["lhip", "lknee"], ["lknee", "lankle"], ["neck", "nose"], ["nose", "reye"],
                 ["reye", "rear"], ["nose", "leye"], ["leye", "lear"] ]

    #pretrained model of pose estimation
    net = cv2.dnn.readNetFromTensorflow("../config/graph_opt.pb")

    #methods ---------------------------------------------------------------------------------------------------------
    
    def detect(self, frame):
        frame_width  = frame.shape[1]
        frame_height = frame.shape[0]
        blob = cv2.dnn.blobFromImage(frame, 1.0, (368,368), (127.5, 127.5, 127.5), swapRB=True, crop=False)
        self.net.setInput(blob)
        out = self.net.forward()
        out = out[:, :19, :, :]  # Mobile net output [1, 57, -1, -1], we only need the first 19 elements

        assert(len(self.BODY_PARTS) == out.shape[1])

        points = []
        for i in range(len(self.BODY_PARTS)):
            # slice heatmap of corresponging body's part.
            heatMap = out[0, i, :, :]

            # originally, we try to find all the local maximums. To simplify a sample
            # we just find a global one. However only a single pose at the same time
            # could be detected this way.
            _, conf, _, point = cv2.minMaxLoc(heatMap)
            x = (frame_width * point[0]) / out.shape[3]
            y = (frame_height * point[1]) / out.shape[2]
            # add a point if it's confidence is higher than threshold.
            points.append((int(x), int(y)) if conf > 0.2 else None)

        sequence = []
        for pair in self.POSE_PAIRS:
            key = pair[0]
            value = pair[1]
            assert(key in self.BODY_PARTS)
            assert(value in self.BODY_PARTS)

            idFrom = self.BODY_PARTS[key]
            idTo = self.BODY_PARTS[value]

            if points[idFrom] and points[idTo]:
                sequence.append(idFrom)
                sequence.append(idTo)

                if len(sequence) == 4:
                    if sequence[0] == 2 and sequence[1] == 3 and sequence[2] == 3 and sequence[3] == 4:
                        cv2.putText(frame, 'Right arm', (10,20), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,255), 2, cv2.LINE_AA)
                    if sequence[0] == 5 and sequence[1] == 6 and sequence[2] == 6 and sequence[3] == 7:
                        cv2.putText(frame, 'Left arm',  (10,60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,255), 2, cv2.LINE_AA)
                    if sequence[0] == 8 and sequence[1] == 9 and sequence[2] == 9 and sequence[3] == 10:
                        cv2.putText(frame, 'Right leg', (10,90), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,255), 2, cv2.LINE_AA)
                    if sequence[0] == 11 and sequence[1] == 12 and sequence[2] == 12 and sequence[3] == 13:
                        cv2.putText(frame, 'Left leg',  (10,120), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,255), 2, cv2.LINE_AA)

                    sequence = []

                cv2.line(frame, points[idFrom], points[idTo], (100, 100, 0), 3)
                cv2.ellipse(frame, points[idFrom], (3, 3), 0, 0, 360, (0, 0, 255), cv2.FILLED)
                cv2.ellipse(frame, points[idTo],   (3, 3), 0, 0, 360, (0, 0, 255), cv2.FILLED)
                        
        return frame
