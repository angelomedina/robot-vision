#Services
from services.openVideo import OpenVideo
#Components
from components.age_gender import AgeGender
from components.people import People
from components.pose import Pose
from components.sport import Sport
#Modules
import cv2
import os

class RealTimeVideo:
    font                   = cv2.FONT_HERSHEY_SIMPLEX
    locationOfText = (0,25)
    fontScale              = 0.5
    fontColor              = (255,255,255)
    lineType               = 2
    def __init__(self):
        #Video
        self.pathc1=0
        self.pathv1="C:/Users/Juan Valerio/Documents/Projects/IAP/robot-vision/assets/End_of_a_jam.ogv"
        self.pathv2="C:/Users/Juan Valerio/Documents/Projects/IAP/robot-vision/assets/v_Rafting_g19_c05.avi"
        self.pathv3="C:/Users/Juan Valerio/Documents/Projects/IAP/robot-vision/assets/v_Drumming_g23_c07.avi"
        self.pathv4="C:/Users/Juan Valerio/Documents/Projects/IAP/robot-vision/assets/v_FloorGymnastics_g18_c01.avi"
        self.pathv5="C:/Users/Juan Valerio/Documents/Projects/IAP/robot-vision/assets/walking.mp4"
        self.path=0
        self.cap = OpenVideo(self.path)
        #Detections
        self.ageGenderDetection=False
        self.ageGenderDetector=AgeGender()
        self.peopleDetection=False
        self.peopleDetector=People()
        self.poseDetection=False
        self.poseDetector=Pose()
        self.sportDetection=False
        self.sportDetector=Sport()

    def ShowVideo(self):
        self.cap.release()
        cv2.destroyAllWindows()
        self.cap=OpenVideo(self.path)
        while True:
            hasFrame, frame = self.cap.read()#bg coor formart
            if(not hasFrame):
                break
            frame = self.detections(frame)
            cv2.imshow("Frame", frame)
            
            if frame is None:
                break
            key = cv2.waitKey(1)
            #keys
            if key == 27:
                break
        self.cap.release()
        cv2.destroyAllWindows()

    def ShowVideo2(self, message):
        self.cap.release()
        self.cap=OpenVideo(self.path)
        while True:
            hasFrame, frame = self.cap.read()#bg coor formart
            if(not hasFrame):
                break
            cv2.putText(frame, str(message), 
            self.locationOfText, 
            self.font, 
            self.fontScale,
            self.fontColor,
            self.lineType)
            cv2.imshow("Frame", frame)
            key = cv2.waitKey(25)
            #keys
            if key == 27:
                break
        self.cap.release()
        cv2.destroyAllWindows()
    
    def detections(self, frame):
        if(self.ageGenderDetection == True):
            frame = self.ageGenderDetector.detect(frame)
        if(self.peopleDetection == True):
            frame = self.peopleDetector.detect(frame)
        if(self.poseDetection == True):
            frame = self.poseDetector.detect(frame)
        return frame

    def C1(self):
        self.path = self.pathc1
        print("Camera")
    def V1(self):
        self.path = self.pathv1
        print("End of a jam")
    def V2(self):
        self.path = self.pathv2
        print("Rafting")
    def V3(self):
        self.path = self.pathv3
        print("Drumming")
    def V4(self):
        self.path = self.pathv4
        print("Floor Gymnastics")
    def V5(self):
        self.path = self.pathv5
        print("Walking")

    #Enable/Disable Detections
    def TurnAgeGenderDetection(self):
        self.ageGenderDetection = not self.ageGenderDetection
    
    def TurnPeopleDetection(self):
        self.peopleDetection = not self.peopleDetection

    def TurnPoseDetection(self):
        self.poseDetection = not self.poseDetection
    
    def TurnSportDetection(self):
        if(self.path == 0):
            print("Debe seleccionar un video, la cámara no se encuentra disponible para este algoritmo.")
            return
        #self.sportDetection = not self.sportDetection
        top1 = self.sportDetector.detect(self.path)
        self.ShowVideo2(top1)
        

