import cv2
import math
import argparse

class AgeGender:

    #attributes
    face_proto   = "../config/opencv_face_detector.pbtxt"
    face_model   = "../config/opencv_face_detector_uint8.pb"
    age_proto    = "../config/age_deploy.prototxt"
    age_model    = "../config/age_net.caffemodel"
    gender_proto = "../config/gender_deploy.prototxt"
    gender_model = "../config/gender_net.caffemodel"

    MODEL_MEAN_VALUES = (78.4263377603, 87.7689143744, 114.895847746)
    age_list    = ['(0-2)', '(4-6)', '(8-12)', '(15-20)','(21-24)', '(25-32)', '(38-43)', '(48-53)', '(60-100)']
    gender_list = ['Male','Female']

    face_net   = cv2.dnn.readNet(face_model,face_proto)
    age_net    = cv2.dnn.readNet(age_model,age_proto)
    gender_net = cv2.dnn.readNet(gender_model,gender_proto)

    path = ""

    #constructor
    #def __init__(self):

    def draw(self, net, frame, conf_threshold = 0.7):
        #deep neural network (dnn)
        image = frame.copy()
        frame_height = image.shape[0]
        frame_width = image.shape[1]
        #predictions from deep neural networks (blod)
        blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300), [104, 117, 123], True, False)
        net.setInput(blob)
        detections = net.forward()
        face_boxes = []
        for i in range(detections.shape[2]):
            confidence = detections[0,0,i,2]
            if confidence > conf_threshold:
                x1 = int(detections[0,0,i,3]*frame_width)
                y1 = int(detections[0,0,i,4]*frame_height)
                x2 = int(detections[0,0,i,5]*frame_width)
                y2 = int(detections[0,0,i,6]*frame_height)
                face_boxes.append([x1,y1,x2,y2])
                cv2.rectangle(image, (x1,y1), (x2,y2), (0,255,0), int(round(frame_height/150)), 8)
        return image,face_boxes

    def detect(self, frame):
        result_img,face_boxes = self.draw(self.face_net,frame)
        padding = 30
        #if not face_boxes:
            #print("No face detected")
        for item in face_boxes:
            face = frame[max(0,item[1]-padding):
                    min(item[3]+padding,frame.shape[0]-1),max(0,item[0]-padding)
                    :min(item[2]+padding, frame.shape[1]-1)]
            #predict gender
            blob = cv2.dnn.blobFromImage(face, 1.0, (227,227), self.MODEL_MEAN_VALUES, swapRB=False)
            self.gender_net.setInput(blob)
            gender_preds = self.gender_net.forward()
            gender = self.gender_list[gender_preds[0].argmax()]
            #predict age
            self.age_net.setInput(blob)
            age_preds = self.age_net.forward()
            age = self.age_list[age_preds[0].argmax()]
                
            cv2.putText(result_img, f'{gender}, {age}', (item[0], item[1]-10), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,255), 2, cv2.LINE_AA)
        return result_img

            